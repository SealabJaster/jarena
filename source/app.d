import std.stdio, std.typecons, std.file, std.json, std.string;

import derelict.sfml2.graphics, derelict.sfml2.system, derelict.sfml2.window;

import jarena.config, jarena.util, jarena.game, jarena.input, jarena.view;

void main()
{
	loadDerelict();

	// Load in the config file(If there is one)
	auto config 	= loadConfig();
	auto mouse 		= refCounted(Mouse());
	auto keyboard 	= refCounted(Keyboard());

	// Setup the window
	auto window = sfRenderWindow_create(sfVideoMode(config.window.width, config.window.height), 
        config.window.title.toStringz(), sfClose, null);
	Util.updateWindow(window, config);

    // Setup the view
    auto view = refCounted(View(window));

	// Setup the clock
	auto 	clock = sfClock_create();
	sfTime 	time;

	auto 	game = new Game(config, mouse, keyboard, view);

	// Free any data when we go out of scope.
	scope(exit)
	{
		sfRenderWindow_destroy(window);
		sfClock_destroy(clock);

		saveConfig(config);
	}

	sfEvent e;
	while(sfRenderWindow_isOpen(window))
	{
		mouse.updateMouseData(window);
		keyboard.updateData();

		while(sfRenderWindow_pollEvent(window, &e))
		{
			switch(e.type)
			{
				case sfEvtClosed:
					sfRenderWindow_close(window);
					break;

				default:
					mouse.handleEvent(e);
					keyboard.handleEvent(e);
					break;
			}
		}
		sfRenderWindow_clear(window, sfWhite);

		// Update the game
		game.update(window, time);
		sfRenderWindow_display(window);

		// Store how long the last frame took
		time = sfClock_restart(clock);
	}
}

void loadDerelict()
{
	DerelictSFML2Graphics.load();
	DerelictSFML2System.load();
	DerelictSFML2Window.load();

	assert(sfRenderWindow_create !is null, 	"Unable to load SFML2 Graphics.");
	assert(sfClock_create !is null,			"Unable to load SFML2 System.");
	assert(sfWindow_create !is null, 		"Unable to load SFML2 Window.");
}

RefConfig loadConfig()
{
	// I want to avoid the GC where I can(As that seems to be good practice, at this point)
    // I need the struct to be used by reference, but it doesn't need to be a class.
    // So I'm just refcounting it.
	auto config = refCounted(Config());

	// If there's a config file, then load it in
	if(exists("Config.json"))
	{
		config.fromJson(parseJSON(readText("Config.json")).object);
	}
	else
	{
		saveConfig(config);
	}

	return config;
}

void saveConfig(RefConfig config)
{
	Util.log("saveConfig", LogLevel.Info, "Saving config file.");

	std.file.write("Config.json", config.toJson());
}