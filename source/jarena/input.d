﻿module jarena.input;

private
{
	// This publicly imports everything I need
	import jarena.object;
}

/// Descibes the state of a mouse button
private struct MouseButtonState
{
	/// The button is down
	bool pressed 	= false;
	
	/// The button was pressed on this frame
	bool clicked 	= false;
	
	/// Lock used for making sure "clicked" is correct
	bool clickLock 	= false;
}

/// Describes the state of the mouse
struct MouseState
{
	/// The position of the mouse, relative to the window
	sfVector2i  position;

    /// The delta position of the mouse wheel, reset to 0 at the end of each loop.
    /// If > 0 then it was scrolled up.
    /// If < 0 then it was scrolled down.
    float       wheelDelta;
}

/// Handles mouse input
struct Mouse
{
	private
	{
		MouseButtonState[sfMouseButton]	_states;
		MouseState						_mouseState;

		void handleMouseButtons(in sfEvent e)
		{
			// If the mouse button doesn't have a state, then create one.
			if((e.mouseButton.button in this._states) is null)
			{
				this._states[e.mouseButton.button] = MouseButtonState();
			}
			auto state = (e.mouseButton.button in this._states);

			// If a button was pressed, mark it as such. If it's released, mark it as such.
			// Also handle whether a button was just clicked on this frame
			if(e.type == sfEvtMouseButtonPressed)
			{
				state.pressed = true;

				// If the clicked field isn't locked, then it was first pressed down on this frame
				if(!state.clickLock)
				{
					state.clickLock = true;
					state.clicked = true;
				}
			}
			else if(e.type == sfEvtMouseButtonReleased)
			{
				state.pressed 	= false;
				state.clickLock = false;
				state.clicked 	= false;
			}
		}
	}

	public
	{
		/// Handles any events related to the mouse.
		/// 
		/// Parameters:
		/// 	e = The event to handle.
		void handleEvent(in sfEvent e)
		{
			switch(e.type)
			{
				case sfEvtMouseButtonPressed:
				case sfEvtMouseButtonReleased:
					this.handleMouseButtons(e);
					break;

                case sfEvtMouseWheelScrolled:
                    this._mouseState.wheelDelta = e.mouseWheelScroll.delta;
                    break;

				default:
					break;
			}
		}

		/// Updates the data about the mouse.
		/// 
		/// Parameters:
		/// 	window = Used to get the mouse's position, relative to the window
		void updateMouseData(in sfRenderWindow* window)
		{
			this._mouseState.position = sfMouse_getPositionRenderWindow(window);

			// Go over each button state, and turn their "clicked" fields to false if their lock is on.
			//
			// Because, by the time this check is made, and their lock is on, that means it was clicked on
			// the last frame, so we set it as not clicked on this frame.
			foreach(ref state; this._states)
			{
				if(state.clickLock)
				{
					state.clicked = false;
				}
			}

            this._mouseState.wheelDelta = 0;
		}

		/// Resets the button states, useful when changing game states, so no buttons are accidentally activated.
		void clearButtons()
		{
			foreach(key; this._states.keys)
			{
				// We intentionally set the lock, just so it doesn't auto-register a click when we don't want it to.
				this._states[key] = MouseButtonState(false, false, true);
			}
		}

		/// Determine whether a specific mouse button is currently pressed down.
		/// 
		/// Parameters:
		/// 	button = The button to check for.
		/// 
		/// Returns:
		/// 	True if the button is currently pressed down.
		bool isPressed(in sfMouseButton button)
		{
			auto state = (button in this._states);
			if(state is null)
			{
				return false;
			}
			else
			{
				return state.pressed;
			}
		}

		/// Determines if this is the first frame that a specific button has been registered as pressed.
		/// 
		/// Parameters:
		/// 	button = The button to check for.
		/// 
		/// Returns:
		/// 	True if the button was first pressed down this frame.
		bool isClicked(in sfMouseButton button)
		{
			auto state = (button in this._states);
			if(state is null)
			{
				return false;
			}
			else
			{
				return state.clicked;
			}
		}

		/// Get the current state of the mouse
		@property
		MouseState state()
		{
			return this._mouseState;
		}
	}
}
alias RefMouse = RefCounted!(Mouse, RefCountedAutoInitialize.no);

/// Contains the current state of the keyboard, such as what text input the OS has processed to us.
struct Keyboard
{
	private
	{
		dchar[]	_pressed;
	
		void handleKeyPressed(sfEvent e)
		{
			dchar character = e.text.unicode;
			if(character == ' ' || character == '\b' || character > 31)
			{
				this._pressed ~= character;
			}
		}
	}

	public
	{
		/// Handles any events related to the keyboard.
		/// 
		/// Parameters;
		/// 	e = The event to process
		void handleEvent(sfEvent e)
		{
			switch(e.type)
			{
				case sfEvtTextEntered:
					this.handleKeyPressed(e);
					break;

				default: break;
			}
		}

		/// Updates the keyboard's data.
		void updateData()
		{
			this._pressed.length = 0;
		}

		@property
		{
			/// Get the keys that have been inputted via the sfTextEntered event for this frame.
			const(dchar[]) keyInput()
			{
				return this._pressed;
			}
		}
	}
}
alias RefKeyboard = RefCounted!(Keyboard, RefCountedAutoInitialize.no);