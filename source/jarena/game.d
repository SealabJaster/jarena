﻿module jarena.game;

private
{
	import jarena.object, jarena.gamestate;
}

/// The main game class
class Game
{
	private
	{
		GameStateManager	_stateManager;
		RefConfig			_config;
		RefAssetLoader		_assets;
		RefMouse			_mouse;
		RefKeyboard			_keyboard;
        RefView             _view;

		void setupStates()
		{
			this._stateManager = new GameStateManager(this);
			this._stateManager.add!TestingState;
            this._stateManager.add!MapEditorState;
            this._stateManager.add!MainMenuState;

            this._stateManager.swap(GameStates.Menu_Main);
		}
	}

	public
	{
		this(RefConfig config, RefMouse mouse, RefKeyboard keyboard, RefView view)
		{
			// Setup any important things.
			this._assets    = refCounted(AssetLoader());
			this._config    = config;
			this._mouse     = mouse;
			this._keyboard  = keyboard;
            this._view      = view;

			// Setup the state manager
			this.setupStates();

			Util.log("Game::this", LogLevel.Debug, "Config data = %s.", this.config);
		}

		/// Update the game.
		/// 
		/// Parameters:
		/// 	window = The game's window
		/// 	gameTime = The time the last frame took
		void update(sfRenderWindow* window, sfTime gameTime)
		{
			this._stateManager.update(window, gameTime);
		}

		@property
		{
			/// Get the game's state manager.
			GameStateManager stateManager()
			{
				return this._stateManager;
			}

			/// Get the game's config data.
			RefConfig config()
			{
				return this._config;
			}

			/// Get the asset loader for the game.
			RefAssetLoader assets()
			{
				return this._assets;
			}

			/// Get access to the state of the mouse.
			RefMouse mouse()
			{
				return this._mouse;
			}

			/// Get access to the state of the keyboard.
			RefKeyboard keyboard()
			{
				return this._keyboard;
			}

            /// Get access to controlling the view of the game's window.
            RefView view()
            {
                return this._view;
            }
		}
	}
}