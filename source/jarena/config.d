﻿module jarena.config;

private
{
	import std.typecons, std.json;

	import jarena.util;
}

/// Contains configuration data for the game. TODO: Non-static as I plan to make a configuration file.
struct Config
{
	struct Window
	{
		/// Width of the window
		uint 	width 	= 860;

		/// Height of the window
		uint 	height 	= 720;

		/// The title of the window
		string	title 	= "Jaster Arena";

		/// How many frames the window will try to achieve per second
		uint	fps 	= 60;
	}

	struct Fonts
	{
		/// The main font, used in most cases.
		string	main = "Assets/Fonts/Arial.ttf";
	}

    struct Network
    {
        /// The network's default port
        ushort  port = 7777;
    }

	/// Config data for the window
	Window 	window;

	/// Paths to fonts for any use.
	Fonts	fonts;

    /// Data for networking 
    Network network;

	/// Loads data from a JSON file
	/// 
	/// Parameters:
	/// 	head = The main object of the JSON file.
	void fromJson(JSONValue[string] head)
	{
		Util.log("Config::fromJson", LogLevel.Info, "Loading Config file.");

		// Load in the window data
		auto value = ("Window" in head);
		if(value !is null)
		{
			auto data = value.object;

			this.setIfExists!(JSON_TYPE.UINTEGER)	(data, "Width", 	&this.window.width);
			this.setIfExists!(JSON_TYPE.UINTEGER)	(data, "Height", 	&this.window.height);
			this.setIfExists!(JSON_TYPE.UINTEGER)	(data, "FPS", 		&this.window.fps);
			this.setIfExists!(JSON_TYPE.STRING)		(data, "Title",		&this.window.title);
		}

		// Load in the font data
		value = ("Fonts" in head);
		if(value !is null)
		{
			auto data = value.object;

			this.setIfExists!(JSON_TYPE.STRING)	(data, "Main", &this.fonts.main);
		}

        // Load in the networking data
        value = ("Network" in head);
        if(value !is null)
        {
            auto data = value.object;

            this.setIfExists!(JSON_TYPE.UINTEGER) (data, "Port", &this.network.port);
        }
	}

	/// Converts the struct into a pretty JSON form.
	/// 
	/// Returns:
	/// 	Valid JSON representing the config data.
	string toJson()
	{
		Util.log("Config::toJson", LogLevel.Info, "Creating Json from config data");

		JSONValue[string] head;

		// Sort out the window data
		JSONValue[string] windowData;
		windowData["Width"] 	= this.window.width;
		windowData["Height"]	= this.window.height;
		windowData["FPS"]		= this.window.fps;
		windowData["Title"]		= this.window.title;

		// Font paths
		JSONValue[string] fontData;
		fontData["Main"]		= this.fonts.main;

        // Network data
        JSONValue[string] networkData;
        networkData["Port"]     = this.network.port;

		// Then add all of the objects to the head
		head["Window"] 	= windowData;
		head["Fonts"] 	= fontData;
        head["Network"] = networkData;

		JSONValue root = head;
		Util.log("Config::toJson", LogLevel.Debug, "root.type = %s. Final Json Output = '%s'", root.type, head);

		return toJSON(&root, true);
	}

	private 
	{
		/// Sets a field in the struct if it exists in the JSON
		void setIfExists(JSON_TYPE type, T)(JSONValue[string] object, string field, T* data)
		{
			auto value = (field in object);
			if(value !is null)
			{
				Util.log("Config::setIfExists", LogLevel.Debug, 
						 "Loading field '%s' of type '%s'. JSON_TYPE.%s", field, typeid(T), type);

				static if(type == JSON_TYPE.INTEGER || type == JSON_TYPE.UINTEGER)
				{
					if(value.type == JSON_TYPE.INTEGER)
					{
						*data = cast(T)value.integer();
					}
					else
					{
						*data = cast(T)value.uinteger();
					}
				}
				else static if(type == JSON_TYPE.STRING)
				{
					*data = value.str;
				}
				else
				{
					assert(false);
				}
			}
		}
	}
}

alias RefConfig = RefCounted!(Config, RefCountedAutoInitialize.no);