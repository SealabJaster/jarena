﻿module jarena.gui;

private
{
    import std.conv;

	import jarena.object;
}

/// Base class for any gui class.
class GuiObject : GameObject
{
	private
	{
		bool	_active = true;
	}

	public 
	{
		this(Game game)
		{
			super(game);
		}

		/// Centers the gui control into another gui control
		///
		/// Parameters:
		///		object = The object to center into
		///		offset = Offset to apply to the final output, just before it's returned.
		void centerIn(GuiObject object, sfVector2f offset = sfVector2f(0, 0))
		{
			auto thisSize = sfVector2f(this.globalBounds.width, this.globalBounds.height);
			auto otherSize = sfVector2f(object.globalBounds.width, object.globalBounds.height);

			// Floats are auto-initialised to NaN, which butchers things
			if(thisSize == sfVector2f())
			{
				thisSize = sfVector2f(0, 0);
			}
			if(otherSize == sfVector2f())
			{
				otherSize = sfVector2f(0, 0);
			}

			sfVector2f newPosition = object.position;
			newPosition = newPosition.add(otherSize.div(sfVector2f(2, 2)))
			                         .sub(thisSize.div(sfVector2f(2, 2)));

			this.position = newPosition.add(offset);
		}

		@property
		{
			/// Get whether the control is performing any logic.
			bool active()
			{
				return this._active;
			}
			
			/// Set whether the control should peform logic.
			void active(bool set)
			{
				this._active = set;
			}
		}

		abstract
		{
			/// Moves the gui control.
			/// 
			/// Parameters:
			/// 	offset = The offset to move it by.
			void move(sfVector2f offset);

			@property
			{
				/// Get the position of the control.
				sfVector2f position();

				/// Set the position of the control.
				void position(sfVector2f pos);

				/// Get the local bounds of the control.
				sfFloatRect localBounds();

				/// Get the global bounds of the control.
				sfFloatRect globalBounds();

				/// Get the colour of the control.
				sfColor colour();

				/// Set the colour of the control.
				void colour(sfColor newColour);
			}
		}
	}
}

/// Contains any number of gui controls.
/// TODO: Possibly add a way to select draw order, otherwise otherlapping controls can be moobed
class GuiContainer : GuiObject
{
	private
	{
		GuiObject[string]	_objects;
		sfFloatRect			_bounding;
		sfColor				_colour;
	}

	public
	{
		/// Constructs a new GuiContainer.
		/// 
		/// Parameters:
		/// 	game = The game object
		/// 	bounding = The position of the container, and the size of it(See more below).
		///
		/// The bounding box will determine the position and size of the container.
		/// The size of the container limits where the position of the controls can be within the container.
		/// The above limitation is only enforced when adding a control.
		this(Game game, sfFloatRect bounding)
		{
			this._bounding = bounding;
			super(game);
		}

		/// Registers the given control under the specified name.
		/// 
		/// Parameters:
		/// 	object = The object to register.
		/// 	name = The name to associate with the object.
		/// 	relative = If true, then $(B objects) position will be seen as relative to the container's position.
		/// 			   Meaning that the container will use "correctPositionFor" on it.
		void add(GuiObject object, string name, bool relative = true)
		in
		{
			assert(object !is null && name !is null);
			assert((name in this._objects) is null);
		}
		body
		{
			Util.log("GuiContainer::add", LogLevel.Debug, "Adding new GuiObject called '%s'. Relative = %s", 
				     name, relative);

			this._objects[name] = object;

			if(relative)
			{
				this.correctPositionOf(name);
			}

			// Make sure that the position of the object is within the allowed range
			if(object.position.x > (this._bounding.left + this._bounding.width) ||
			   object.position.y > (this._bounding.top  + this._bounding.height))
			{
				// I'll probably make this into an exception later.
				assert(false);
			}
		}

		/// Because the container doesn't support keeping controls' positions relative to the container.
		/// Use this function anytime a control's position is changed outside of the container's functions.
		/// However, only make sure to use this function if the gui's current position is supposed to be relative.
		/// 
		/// Parameters:
		/// 	name = The name of the object to correct.
		void correctPositionOf(string name)
		{
			Util.log("GuiContainer::correctPositionOf", LogLevel.Debug, "Correcting '%s'.", name);

			auto object = this.get!GuiObject(name);
			object.position = object.position.add(this.position);
		}

		/// Get the object assosciated with the specified name.
		/// 
		/// Parameters:
		/// 	name = The name of the object
		/// 
		/// Returns:
		/// 	The object casted to $(B T)
		T get(T : GuiObject)(string name)
		{
			//Util.log("GuiContainer::get", LogLevel.Debug, "Getting control of type '%s' called '%s'", typeid(T), name);

			auto pointer = (name in this._objects);
			if(pointer is null)
			{
				// My game will require every single GUI component to be present for it to work properly.
				// So when the game asks for one that doesn't exist, then it's very clearly an error.
				assert(false);
			}
			else
			{
				return cast(T)*pointer;
			}
		}

		override
		{
			/// Moves the gui container, and all of it's controls.
			/// 
			/// Parameters:
			/// 	offset = The offset to move it by.
			void move(sfVector2f offset)
			{
				this._bounding.left += offset.x;
				this._bounding.top  += offset.y;

				foreach(control; this._objects.byValue)
				{
					control.move(offset);
				}
			}

			/// Update all of the controls in the container.
			/// 
			/// Parameters:
			/// 	window = The game's window
			/// 	gameTime = The time the last frame took
			void update(sfRenderWindow* window, sfTime gameTime)
			{
                // GUI elements shouldn't be manipulated by the custom view, it'd just look strange.
                auto useDefault = this.game.view.useDefaultView;
                this.game.view.useDefaultView = true;

                // Just in case that we don't make it to the end of the function due to future changes or something.
                scope(exit) this.game.view.useDefaultView = useDefault;

                if(this.active)
				{
					foreach(control; this._objects.byValue)
					{
						control.update(window, gameTime);
					}
				}
			}
			
			@property
			{
				/// Get the position of the container.
				sfVector2f position()
				{
					return sfVector2f(this._bounding.left, this._bounding.top);
				}
				
				/// Set the position of the container and it's controls.
				void position(sfVector2f pos)
				{
					/*
					 * pos = 0, 0
					 * this = 100, 0
					 * 
					 * pos - this = -100, 0
					 * 
					 * ------------------------
					 * 
					 * pos = 100, 0
					 * this = 0, 0
					 * 
					 * pos - this = 100, 0
					 * 
					 * */

					this.move(pos.sub(this.position));
				}
				
				/// Get the local bounds of the container.
				sfFloatRect localBounds()
				{
					return this._bounding;
				}
				
				/// Get the global bounds of the container.
				sfFloatRect globalBounds()
				{
					return this._bounding;
				}
				
				/// Get the colour of the container.
				sfColor colour()
				{
					return this._colour;
				}
				
				/// Set the colour of the container's controls.
				void colour(sfColor newColour)
				{
					this._colour = newColour;

					foreach(control; this._objects.byValue)
					{
						control.colour = newColour;
					}
				}
			}
		}
	}
}

/// Contains data about how a label should look
struct LabelData
{
	/// How large the characters of the label are.
	uint	characterSize;

	/// What font the label uses.
	string	font;

	/// What colour the text of the label is.
	sfColor	colour;
}

/// A gui control for showing a piece of text to the screen.
class Label : GuiObject
{
	private
	{
		sfText*	_text;
	}

	public
	{
		/// Constructs a new label, using the given LabelData to describe it's visuals.
		/// 
		/// Parameters:
		/// 	game = The game object
		/// 	data = The way the label should look
		/// 	text = The text the label should show
		/// 	position = Where the label should be on the screen.
		this(Game game, LabelData data, dstring text = "", sfVector2f position = sfVector2f(0, 0))
		{
			super(game);

			this._text = sfText_create();
			sfText_setFont(this._text, game.assets.loadFont(data.font));

			this.characterSize = data.characterSize;
			this.text = text;
			this.position = position;
			this.colour = data.colour;
		}

		~this()
		{
			if(this._text !is null)
			{
				sfText_destroy(this._text);
			}
		}

		@property
		{
			/// Get the size of the label's characters.
			uint characterSize()
			{
				return sfText_getCharacterSize(this._text);
			}

			/// Set the size of the label's characters.
			void characterSize(uint size)
			{
				sfText_setCharacterSize(this._text, size);
			}

			/// Get the label's string
			dstring text()
			{
				// Umm... Am I expected to free this pointer(I'd heavily assume not)
				auto pointer = sfText_getUnicodeString(this._text);
				size_t count = 0;

				while(true)
				{
					dchar next = pointer[count++];

					if(next == '\0')
					{
						break;
					}
				}

				return cast(dstring)pointer[0..count];
			}

			/// Set the label's string
			void text(dstring text)
            {
				sfText_setUnicodeString(this._text, (text.length != 0 && text[$-1] == '\0') 
													? text.ptr 
													: (text ~ '\0').ptr);
			}
		}

		override
		{
			/// Update the object.
			/// 
			/// Parameters:
			/// 	window = The game's window
			/// 	gameTime = The time the last frame took
			void update(sfRenderWindow* window, sfTime gameTime)
			{
				sfRenderWindow_drawText(window, this._text, null);
			}

			/// Moves the gui control.
			/// 
			/// Parameters:
			/// 	offset = The offset to move it by.
			void move(sfVector2f offset)
			{
				sfText_move(this._text, offset);
			}
			
			@property
			{
				/// Get the position of the control.
				sfVector2f position()
				{
					return sfText_getPosition(this._text);
				}
				
				/// Set the position of the control.
				void position(sfVector2f pos)
				{
					sfText_setPosition(this._text, pos);
				}
				
				/// Get the local bounds of the control.
				sfFloatRect localBounds()
				{
					return sfText_getLocalBounds(this._text);
				}
				
				/// Get the global bounds of the control.
				sfFloatRect globalBounds()
				{
					return sfText_getGlobalBounds(this._text);
				}
				
				/// Get the colour of the control.
				sfColor colour()
				{
					return sfText_getColor(this._text);
				}
				
				/// Set the colour of the control.
				void colour(sfColor newColour)
				{
					sfText_setColor(this._text, newColour);
				}
			}
		}
	}
}

/// Label that takes input from the user
class InputLabel : Label
{
	private
	{
		dstring	_data = "";
	}

	public
	{
		this(Game game, LabelData data, sfVector2f position = sfVector2f(0, 0))
		{
			super(game, data, "", position);
		}

        /// Takes off the last character in the input.
        void pop()
        {
            if(this._data.length == 0)
            {
                return;
            }
            else if(this._data[$ - 1] == '|')
            {
                if(this._data.length > 1)
                {
                    this._data = this._data[0..$ - 2];
                }
            }
            else
            {
                this._data = this._data[0..$ - 1];
            }
        }

		override
		{
			/// Update the object.
			/// 
			/// Parameters:
			/// 	window = The game's window
			/// 	gameTime = The time the last frame took
			void update(sfRenderWindow* window, sfTime gameTime)
			{
				bool change = false;

				if(this.active)
				{
					// Process user input
					auto input = this.game.keyboard.keyInput;

					foreach(ch; input)
					{
						change = true;

						// Remove the | if it's there, so it doesn't keep duplicating itself
						if(this._data.length != 0 && this._data[$ - 1] == '|')
						{
							this._data.length -= 1;
						}

						// If the user presses backspace, then remove a character.
						if(ch == '\b')
						{
							this._data.length -= (this._data.length == 0) ? 0 : 1;
						}
						else // Otherwise, add it.
						{
							this._data ~= ch;
						}
					}

					// Just to make it seem more interactive, put a | at the end of it while it's active
                    if(this._data.length == 0 || this._data[$ - 1] != '|')
					{
						this._data ~= '|';
						change = true;
                    }
				}
				else
				{
					// However, if the label is inactive, and the last character is a |, then remove it.
					if(this._data.length != 0 && this._data[$ - 1] == '|')
					{
						this._data.length -= 1;
						change = true;
					}
				}

				// If there was a change in it's text, then update it.
				if(change)
				{
					super.text = this._data;
				}

				super.update(window, gameTime);
			}

			/// Get the label's string
			@property
			dstring text()
			{
				return (this._data.length != 0 && this._data[$ - 1] == '|' ? this._data[0..$ - 1] : this._data);
			}
		}
	}
}

/// Contains data about how a shape should look
struct ShapeData
{
    /// The size of the shape
    sfVector2f  size;

    /// The fill colour of the shape
    sfColor     fillColour;

    /// How thich the shape's outline is.
    uint        thickness;

    /// The colour of the shape's outline
    sfColor     outlineColour;
}

/// Wrapper around sfRectangle
class Rectangle : GuiObject
{
    private
    {
        sfRectangleShape*   _shape;
    }

    public
    {
        /// Constructs a new rectangle.
        /// 
        /// Parameters:
        ///     game = The game object
        ///     data = How the shape should look
        ///     position = The position of the shape
        this(Game game, ShapeData data, sfVector2f position = sfVector2f(0, 0))
        {
            super(game);
            this._shape = sfRectangleShape_create();

            this.position = position;
            this.updateShape(data);
        }

        this(Game game, sfTexture* texture, sfVector2f position = sfVector2f(0, 0))
        in
        {
            assert(texture !is null);
        }
        body
        {
            super(game);
            this._shape = sfRectangleShape_create();

            this.position = position;
            this.texture = texture;
        }

        /// Changes the shape to match the given data.
        /// 
        /// Parameters:
        ///     data = How the shape should look
        void updateShape(ShapeData data)
        {
            sfRectangleShape_setSize(this._shape, data.size);
            sfRectangleShape_setFillColor(this._shape, data.fillColour);
            sfRectangleShape_setOutlineThickness(this._shape, data.thickness);
            sfRectangleShape_setOutlineColor(this._shape, data.outlineColour);
        }

        ~this()
        {
            if(this._shape !is null)
            {
                sfRectangleShape_destroy(this._shape);
            }
        }

        @property
        {
            /// Get the outline colour of the rectangle.
            sfColor outlineColour()
            {
                return sfRectangleShape_getOutlineColor(this._shape);
            }
            
            /// Set the fill outline of the rectangle.
            void outlineColour(sfColor newColour)
            {
                sfRectangleShape_setOutlineColor(this._shape, newColour);
            }

            /// Set the rectangle's texture
            /// NOTE: Resets the rectangle's size
            void texture(sfTexture* texture)
            {
                sfRectangleShape_setTexture(this._shape, texture, true);
                sfRectangleShape_setSize(this._shape, sfVector2f(0, 0).add(sfTexture_getSize(texture)));
            }

            /// Get the rectangle's texture
            const(sfTexture*) texture()
            {
                return sfRectangleShape_getTexture(this._shape);
            }
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                sfRenderWindow_drawRectangleShape(window, this._shape, null);
            }
            
            /// Moves the gui control.
            /// 
            /// Parameters:
            ///     offset = The offset to move it by.
            void move(sfVector2f offset)
            {
                sfRectangleShape_move(this._shape, offset);
            }
            
            @property
            {
                /// Get the position of the control.
                sfVector2f position()
                {
                    return sfRectangleShape_getPosition(this._shape);
                }
                
                /// Set the position of the control.
                void position(sfVector2f pos)
                {
                    sfRectangleShape_setPosition(this._shape, pos);
                }
                
                /// Get the local bounds of the control.
                sfFloatRect localBounds()
                {
                    return sfRectangleShape_getLocalBounds(this._shape);
                }
                
                /// Get the global bounds of the control.
                sfFloatRect globalBounds()
                {
                    return sfRectangleShape_getGlobalBounds(this._shape);
                }
                
                /// Get the fill colour of the rectangle.
                sfColor colour()
                {
                    return sfRectangleShape_getFillColor(this._shape);
                }
                
                /// Set the fill colour of the rectangle.
                void colour(sfColor newColour)
                {
                    sfRectangleShape_setFillColor(this._shape, newColour);
                }
            }
        }
    }
}

/// A rectangle that controls an input label
class Textbox : Rectangle
{
    private
    {
        InputLabel  _input;
        bool        _selected = false;
    }

    public
    {
        this(Game game, ShapeData data, LabelData labelData, sfVector2f position = sfVector2f(0, 0))
        {
            super(game, data, position);
            this._input = new InputLabel(game, labelData, position);
            this._input.active = false;
        }

        @property
        {
            /// Get the textbox's input label
            InputLabel input()
            {
                return this._input;
            }
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                // Activate/deactivate the input label as needed
                auto bounds = this.globalBounds;
                auto mousePos = this.game.view.toWorldPixel(this.game.mouse.state.position);
                if(this.active)
                {
                    // Handle the mouse clicks
                    if(this.game.mouse.isClicked(sfMouseLeft))
                    {
                        this._selected = cast(bool)sfFloatRect_contains(&bounds, mousePos.x, mousePos.y);
                        this._input.active = this._selected;
                    }
                }
                else
                {
                    this._input.active = false;
                }

                // Draw everything
                super.update(window, gameTime);
                this._input.centerIn(this);
                this._input.update(window, gameTime);

                // Keep the text within the textbox
                auto inputBounds = this._input.globalBounds;
                if(inputBounds.left < bounds.left ||
                  (inputBounds.left + inputBounds.width) >= (bounds.left + bounds.width))
                {
                    this._input.pop();
                }
            }
        }
    }
}

/// Delegate for buttons.
alias ButtonEvent = void delegate(Button caller);

/// A gui control that calls a function when it is clicked
class Button : Rectangle
{
    private
    {
        ButtonEvent     _event;
        Label           _text;

        sfColor         _idle, _hover, _click;
        const(float)    _idleMod = 0.75f, _hoverMod = 1f, _clickMod = 0.50f; // These shouldn't be in the config file
    
        this(Game game, LabelData data, dstring text, sfVector2f position, ButtonEvent event,
             sfTexture* texture, ShapeData shape)
        in
        {
            assert(event !is null);
        }
        body
        {
            if(texture !is null)
            {
                super(game, texture, position);
            }
            else
            {
                super(game, shape, position);
            }

            this._text = new Label(game, data, text);
            this._text.centerIn(this);

            this.event = event;
        }
    }

    public
    {
        /// Construct a button.
        /// 
        /// Parameters:
        ///     game = The game object
        ///     shape = How the button should look
        ///     label = How the button's label should look
        ///     text = What the button's label should say
        ///     event = What function the button should call when it is clicked
        this(Game game, ShapeData shape, LabelData label, dstring text, ButtonEvent event, 
             sfVector2f position = sfVector2f(0, 0))
        {
            this(game, label, text, position, event, null, shape);

            // Create the 3 colours that the button uses to be more interactive
            auto colour = shape.fillColour;
            sfColor createColour(inout float modifier)
            {
                return sfColor(cast(ubyte)(colour.r * modifier), cast(ubyte)(colour.g * modifier), 
                               cast(ubyte)(colour.b * modifier), colour.a);
            }

            this._idle  = createColour(this._idleMod);
            this._hover = createColour(this._hoverMod);
            this._click = createColour(this._clickMod);
        }

        /// Construct a button.
        /// 
        /// Parameters:
        ///     game = The game object
        ///     texture = The texture of the button
        ///     label = How the button's label should look
        ///     text = What the button's label should say
        ///     event = What function the button should call when it is clicked
        this(Game game, sfTexture* texture, LabelData label, dstring text, ButtonEvent event, 
            sfVector2f position = sfVector2f(0, 0))
        {
            this(game, label, text, position, event, texture, ShapeData());

            // Create the colours that the button uses
            this._idle = sfColor(174, 174, 174, 0xFF);
            this._hover = sfColor(0xFF, 0xFF, 0xFF, 0xFF);
            this._click = sfColor(128, 128, 128, 0xFF);
        }

        @property
        {
            /// Get the button's text
            Label text()
            {
                return this._text;
            }

            /// Get the button's event
            ButtonEvent event()
            {
                return this._event;
            }

            /// Set the button's event
            void event(ButtonEvent evnt)
            {
                this._event = evnt;
            }
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                this._text.centerIn(this, sfVector2f(0, -this._text.globalBounds.height / 3));

                // If the mouse is over and is clicked, then run the event(If there is one)
                auto bounds = this.globalBounds;
                auto mouse = this.game.view.toWorldPixel(this.game.mouse.state.position);
                if(cast(bool)sfFloatRect_contains(&bounds, mouse.x, mouse.y))
                {
                    if(this.game.mouse.isClicked(sfMouseLeft))
                    {
                        this.colour = this._click;

                        if(this._event !is null)
                        {
                            this._event(this);
                        }
                    }
                    else
                    {
                        this.colour = this._hover;
                    }
                }
                else
                {
                    this.colour = this._idle;
                }

                super.update(window, gameTime);
                this._text.update(window, gameTime);
            }

            /// Moves the gui control.
            /// 
            /// Parameters:
            ///     offset = The offset to move it by.
            void move(sfVector2f offset)
            {
                super.move(offset);
                this._text.move(offset);
            }
        }
    }
}

/// Self-updating label that displays the current fps.
class FPSCounter : Label
{
    private
    {  
        Timer   _timer;
        uint    _count = 0;
    }

    public
    {
        this(Game game, LabelData data, sfVector2f position = sfVector2f(0, 0))
        {
            super(game, data, "0", position);
            this._timer.timeUntilTick = 1;
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                this._timer.update(gameTime);
                if(this._timer.tick)
                {
                    super.text  = to!dstring(this._count);
                    this._count = 0;
                }

                //Util.log("FPSCounter::update", LogLevel.Debug, "FPS = %s | Timer = %s", this._count, this._timer.elapsed);

                this._count += 1;
                super.update(window, gameTime);
            }
        }
    }
}

// This is better suited to the methods a GuiObject needs.
/// A class that provides access to a spritesheet.
/// This includes animation along the x axis.
/// You can change the y axis manually to change which sprites the object shows.
class SpriteSheet : GuiObject
{
    private
    {
        sfVector2u  _frameSize;
        sfVector2u  _frameCount;
        sfVector2u  _currentFrame;
        sfSprite*   _sprite;

        Timer       _timer;
        bool        _animate = false;

        void updateFrame()
        {
            auto frame = this._currentFrame.mul(this._frameSize);
            sfSprite_setTextureRect(this._sprite, sfIntRect(frame.x, frame.y, this._frameSize.x, this._frameSize.y));

//            Util.log("SpriteSheet:updateFrame", LogLevel.Debug, "frame = %s | frameSize = %s | current = %s",
//                        frame, this._frameSize, this._currentFrame);
        }
    }

    public
    {
        /// Create a SpriteSheet using the given texture
        /// 
        /// Parameters:
        ///     game = The game object
        ///     texture = The texture to use as the sprite sheet
        ///     frameSize = The size of each frame in the sprite sheet
        this(Game game, sfTexture* texture, sfVector2u frameSize)
        {
            this._sprite = sfSprite_create();
            sfSprite_setTexture(this._sprite, texture, true);

            // Calculate how many frames are along the x and y axis.
            auto imageSize = sfTexture_getSize(texture);
            this._frameSize = frameSize;
            this._frameCount = imageSize.div(frameSize);
        
            this._currentFrame = sfVector2u(0, 0);
            this.updateFrame();
        
            super(game);
        }

        ~this()
        {
            if(this._sprite !is null)
            {
                sfSprite_destroy(this._sprite);
            }
        }

        @property
        {
            /// Set the current frame on the X axis.
            void frameX(uint x)
            in
            {
                assert(x < this._frameCount.x);
            }
            body
            {
                this._currentFrame.x = x;
                this.updateFrame();
            }

            /// Set the current frame on the Y axis.
            void frameY(uint y)
            in
            {
                assert(y < this._frameCount.y);
            }
            body
            {
                this._currentFrame.y = y;
                this.updateFrame();
            }

            /// Get the current X frame
            uint frameX()
            {
                return this._currentFrame.x;
            }

            /// Get the current Y frame
            uint frameY()
            {
                return this._currentFrame.y;
            }

            /// Set whether the sprite is currently mirrored.
            void inverted(bool invert)
            {
                sfSprite_setScale(this._sprite, sfVector2f((invert) ? -1 : 0, 0));
            }

            /// Get whether the sprite is currently mirrored.
            bool inverted()
            {
                return (sfSprite_getScale(this._sprite).x == -1);
            }
        }

        /// Begins to animate the sprite sheet on the current y axis, along the x axis.
        /// 
        /// Parameters:
        ///     time = How many seconds until the next frame should show
        void start(float time)
        {
            this._timer.timeUntilTick   = time;
            this._timer.elapsed         = 0;

            this._animate = true;
        }

        /// Stops the sprite sheet from animating, and reverts the sprite to a specifed frame.
        /// 
        /// Parameters:
        ///     frame = The frame to set the sprite to.
        void stop(sfVector2u frame)
        {
            this._animate       = false;
            this._currentFrame  = frame;

            this.updateFrame();
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                this._timer.update(gameTime);

                if(this._animate && this._timer.tick)
                {
                    this._currentFrame.x += 1;
                    if(this._currentFrame.x >= this._frameCount.x)
                    {
                        this._currentFrame.x = 0;
                    }

                    this.updateFrame();
                }

                sfRenderWindow_drawSprite(window, this._sprite, null);
            }

            /// Moves the gui control.
            /// 
            /// Parameters:
            ///     offset = The offset to move it by.
            void move(sfVector2f offset)
            {
                sfSprite_move(this._sprite, offset);
            }
            
            @property
            {
                /// Get the position of the control.
                sfVector2f position()
                {
                    return sfSprite_getPosition(this._sprite);
                }
                
                /// Set the position of the control.
                void position(sfVector2f pos)
                {
                    sfSprite_setPosition(this._sprite, pos);
                }
                
                /// Get the local bounds of the control.
                sfFloatRect localBounds()
                {
                    return sfSprite_getLocalBounds(this._sprite);
                }
                
                /// Get the global bounds of the control.
                sfFloatRect globalBounds()
                {
                    return sfSprite_getGlobalBounds(this._sprite);
                }
                
                /// Get the colour of the control.
                sfColor colour()
                {
                    return sfSprite_getColor(this._sprite);
                }
                
                /// Set the colour of the control.
                void colour(sfColor newColour)
                {
                    sfSprite_setColor(this._sprite, newColour);
                }
            }
        }
    }
}