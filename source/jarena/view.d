﻿module jarena.view;

private
{
    import std.typecons;

    import jarena.object;
}

/// Contains data for the bounds of something
/// VERY IMPORTANT NOTE: The x axis is also inverted
struct Bounds
{
    /// Lowest the X can go
    float minX;

    /// Lowest the Y can go
    float minY;

    /// Highest the X can go
    float maxX;

    /// Highest the Y can go
    float maxY;
}

/// A struct to manipulate the view of the window
struct View
{
    private
    {
        sfRenderWindow* _window;
        sfView*         _defaultView;
        sfView*         _view;
        Bounds          _bounds;

        bool            _usingDefault = false;
    }

    public
    {
        this(sfRenderWindow* window)
        {
            this._window        = window;
            this._view          = sfView_copy(sfRenderWindow_getView(this._window));
            this._defaultView   = sfView_copy(this._view);
        }

        ~this()
        {
            if(this._view !is null)
            {
                sfView_destroy(this._view);
            }

            if(this._defaultView !is null)
            {
                sfView_destroy(this._defaultView);
            }
        }

        /// Moves the view of the window, keeping it in bounds.
        /// 
        /// Parameters:
        ///     offset = How much to move the view by
        void move(sfVector2f offset)
        {
            sfView_move(this._view, offset);

            // Keep the view in bounds
            auto center = sfView_getCenter(this._view);
            auto size   = sfView_getSize(this._view).div(sfVector2f(2, 2));

            //Util.log("View::move", LogLevel.Debug, "Center = %s | Size = %s | Bounds = %s", center, size, this.bounds);

            /// If position < bound then move the view to make sure we don't go past bound
            /// We can use the isY flag to keep in bounds for the y axis
            /// And we can use the invert flag for handling with different sides of the view
            void keepInBounds(bool isY, bool invert)(float position, float bound)
            {
                //Util.log("View:move:keepInBounds", LogLevel.Debug, "position = %s | bound = %s", position, bound);
                if(position < bound)
                {
                    auto offset = (position - bound);
                    static if(invert)
                    {
                        offset = -offset;
                    }

                    static if(isY)
                        sfView_move(this._view, sfVector2f(0, offset));
                    else
                        sfView_move(this._view, sfVector2f(offset, 0));
                }
            }

            keepInBounds!(false, true)  ((center.x - size.x), this.bounds.minX);
            keepInBounds!(true,  true)  ((center.y - size.y), this.bounds.minY);
            keepInBounds!(false, false) (this.bounds.maxX, (center.x + size.x));
            keepInBounds!(true,  false) (this.bounds.maxY, (center.y + size.y));

            sfRenderWindow_setView(this._window, this._view);
        }

        /// Converts a world position, to a position relative to a pixel on the window.
        /// 
        /// Parameters:
        ///     position = The position to convert
        ///     
        /// Returns:
        ///     A pixel equivalent to the world position.
        sfVector2i toWindowPixel(sfVector2f position)
        {
            return sfRenderWindow_mapCoordsToPixel(this._window, position, null);
        }

        /// Converts a pixel relative to the window into a world position.
        /// 
        /// Parameters:
        ///     position = The pixel to convert.
        ///     
        /// Returns:
        ///     A world position equialent to the pixel.
        sfVector2f toWorldPixel(sfVector2i position)
        {
            return sfRenderWindow_mapPixelToCoords(this._window, position, null);
        }

        @property
        {
            /// Set the bounds of the view
            void bounds(Bounds bound)
            {
                //Util.log("View:bounds", LogLevel.Info, "View bounds set to %s", bound);
                this._bounds = bound;
                //Util.log("View:bounds", LogLevel.Debug, "Bounds = %s", this._bounds);
            }

            /// Get the bounds of the view
            Bounds bounds()
            {
                return this._bounds;
            }

            /// Set whether the game's window is using the default view, or the custom view.
            void useDefaultView(bool use)
            {
                this._usingDefault = use;
                sfRenderWindow_setView(this._window, (use) ? this._defaultView : this._view);
            }

            /// Get whether the game's window is using the default view, or the custom view
            bool useDefaultView()
            {
                return this._usingDefault;
            }
        }
    }
}

alias RefView = RefCounted!(View, RefCountedAutoInitialize.no);