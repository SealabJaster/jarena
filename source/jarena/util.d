﻿module jarena.util;

private
{
	import std.stdio, std.typecons, std.path, std.format;

	import derelict.sfml2.graphics, derelict.sfml2.system;

	import jarena.config;
}

/// Describes what context the log entry is in.
enum LogLevel
{
	/// General information
	Info,

	/// Debug information
	Debug,

	/// Error text
	Error
}

/// Contains many utility methods.
static class Util
{
	public static
	{
		/// Updates the given render window using data from the config
		/// 
		/// Parameters:
		/// 	window = The render window
		/// 	config = The config to update with window with
		void updateWindow(sfRenderWindow* window, const(RefConfig) config) nothrow
		{
			sfRenderWindow_setFramerateLimit(window, config.window.fps);
			sfRenderWindow_setSize(window, sfVector2u(config.window.width, config.window.height));
			sfRenderWindow_setTitle(window, (config.window.title ~ '\0').ptr);
		}

		/// Constructs a small log message and prints it to stdout
		///
		/// Parameters:
		/// 	caller = How the function calling this method identifies itself
		/// 	level = What kind of info does this log contain
		/// 	format = The format of the log
		/// 	params = The extra parameters for the message
		void log(T...)(const(string) caller, LogLevel level, const(string) format, T params)
		{
			debug{}
			else
			{
				// If we're not in debug mode, don't print out debug logs
				if(level == LogLevel.Debug)
				{
					return;
				}
			}

			writefln("%s > [%s]: " ~ format, caller, level, params);
		}
	}
}

/// A simple timer for any other use.
struct Timer
{
    /// How many seconds until the timer should return true for "tick".
    float timeUntilTick = 0;

	/// How many seconds have elapsed
	float elapsed = 0;

    /// Returns true if "elapsed" has elapsed "timeUntilTick" and resets "elapsed"
    @nogc @property
    bool tick() nothrow
    {
        if(this.timeUntilTick < this.elapsed)
        {
            this.elapsed = 0;
            return true;
        }

        return false;
    }

	/// Update the timer
	@nogc
	void update(sfTime gameTime) nothrow
	{
		this.elapsed += sfTime_asSeconds(gameTime);
	}
}

/// A helper struct to load and cache any data.
struct AssetLoader
{
	private
	{
		struct Store
		{
			sfTexture*	texture;
			sfFont*		font;
		}

		Store[string]	_cache;

        auto load(string T, string storeName, bool secondNull)(string path)
        {
            // Make sure the path is absolute
            path = absolutePath(path) ~ '\0';

            // If it's  been cached, retrieve and return it from the cache, otherwise, load and cache it.
            auto pointer = (path in this._cache);
            if(pointer is null)
            {
                Util.log("AssetLoader::load", LogLevel.Info, "Loading and caching %s from file '%s'.", T, path);

                // Load it in
                static if(secondNull)
                {
                    mixin(format("auto data = %s_createFromFile(path.ptr, null);", T));
                }
                else
                {
                    mixin(format("auto data = %s_createFromFile(path.ptr);", T));
                }

                // Cache it, and then return it
                auto store = Store();
                mixin("store."~storeName~" = data;");

                this._cache[path] = store;
                
                return data;
            }
            else
            {
                Util.log("AssetLoader::load", LogLevel.Info, "Getting %s from cache['%s'].", T, path);
                mixin("return pointer."~storeName~";");
            }
        }
	}

	public
	{
		/// Loads/caches/uncaches the texture at the given path.
		/// 
		/// Parameters:
		/// 	path = The path to the texture.
		/// 
		/// Returns:
		/// 	The sfTexture* of the texture.
        alias loadTexture = load!("sfTexture", "texture", true);

		/// Loads/caches/uncaches the font at the given path.
		/// 
		/// Parameters:
		/// 	path = The path to the font.
		/// 
		/// Returns:
		/// 	The sfFont* of the font.
		alias loadFont = load!("sfFont", "font", false);

		/// Frees the cache's data
		~this()
		{
			foreach(store; this._cache)
			{
				if(store.texture !is null)
				{
					sfTexture_destroy(store.texture);
				}
				else if(store.font !is null)
				{
					sfFont_destroy(store.font);
				}
			}
		}
	}
}

alias RefAssetLoader = RefCounted!(AssetLoader, RefCountedAutoInitialize.no);

@nogc
pure nothrow
{
    /// Determines whether the given type is an SFML vector
    bool isVector(T)()
    {
        return
            (
                is(T == sfVector2f) ||
                is(T == sfVector2i) ||
                is(T == sfVector2u)
            );
    }

    /// Add 2 vectors together.
    /// 
    /// Parameters:
    /// 	vect1 = The first vector.
    /// 	vect2 = The second vector.
    /// 
    /// Returns:
    /// 	The type of the vector is T. returns vect1 and vect2 added together.
    T add(T, T2)(T vect1, T2 vect2)
    if(isVector!T && isVector!T2)
    {
    	return T(vect1.x + vect2.x, vect1.y + vect2.y);
    }

    /// Subtract 2 vectors from eachother.
    /// 
    /// Parameters:
    /// 	vect1 = The first vector.
    /// 	vect2 = The second vector.
    /// 
    /// Returns:
    /// 	The type of the vector is T.
    T sub(T, T2)(T vect1, T2 vect2)
    if(isVector!T && isVector!T2)
    {
    	return T(vect1.x - vect2.x, vect1.y - vect2.y);
    }

    /// Divides 2 vectors.
    /// 
    /// Parameters:
    /// 	vect1 = The first vector.
    /// 	vect2 = The second vector.
    /// 
    /// Returns:
    /// 	The type of the vector is T.
    T div(T, T2)(T vect1, T2 vect2)
    if(isVector!T && isVector!T2)
    {
    	return T(vect1.x / vect2.x, vect1.y / vect2.y);
    }

    /// Multiplies 2 vectors.
    /// 
    /// Parameters:
    /// 	vect1 = The first vector.
    /// 	vect2 = The second vector.
    /// 
    /// Returns:
    /// 	The type of the vector is T.
    T mul(T, T2)(T vect1, T2 vect2)
    if(isVector!T && isVector!T2)
    {
    	return T(vect1.x * vect2.x, vect1.y * vect2.y);
    }
}