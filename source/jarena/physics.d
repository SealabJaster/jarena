﻿module jarena.physics;

public
{
    import jarena.object;
}

/// Indicates a direction
enum Direction : ubyte
{
    /// Used when the other directions can't be
    None = 0,

    Up      = 1,
    Right   = 2,
    Left    = 3,
    Down    = 4
}

/// Static class holding any function related to the game's physics.
/// This can be static since it simply holds functions, and doesn't need to be in an instance.
static class Physics
{
    public static
    {
        /// Determines whether there is a collision with rect1 and rect2.
        /// The intersection of the collision is stored into intersection.
        /// 
        /// Parameters:
        ///     rect1 = The first rectangle
        ///     rect2 = The second rectangle
        ///     intersection = The intersection of the two rectangles.
        ///     
        /// Returns:
        ///     Whether there is a collision
        bool isCollision(in sfFloatRect rect1, in sfFloatRect rect2, sfFloatRect* intersection = null)
        {
            // Yes, this is just a slightly nicer way to use the below function <3
            return cast(bool)sfFloatRect_intersects(&rect1, &rect2, intersection);
        }

        /// Applies $(B movement) to the position of $(B moving), and returns whether it is colliding with $(B colliding) or not.
        /// Convinience function.
        /// 
        /// Parameters:
        ///     moving       = The moving body.
        ///     colliding    = The body to check a collision for.
        ///     movement     = The movement to apply to $(B moving)
        ///     intersection = The intersection of the two rectangles.
        ///     
        /// Returns:
        ///     Whether $(B moving) would collide with $(B colliding) when $(B movement) is applied to $(B moving).
        bool wouldCollide(in sfFloatRect moving, in sfFloatRect colliding, in sfVector2f movement, 
                            sfFloatRect* intersection = null)
        {
            return cast(bool)sfFloatRect_contains(&colliding, moving.left + movement.x, moving.top + movement.y);
        }
    }
}