﻿module jarena.map;

private
{
    import std.algorithm, std.bitmanip;

    import jarena.object, jarena.gui;
}

/// Base class for any class used in a map(Tiles, the players, etc.).
class MapObject : GameObject
{
    private
    {
    }

    public
    {
        this(Game game)
        {
            super(game);
        }
    }

    protected
    {
        /// Determines whether the rectangle is currently somewhere on screen.
        /// 
        /// Parameters:
        ///     bounds = The rectangle to check
        ///     completely = If true, this function will only be true if every pixel of the thing is on screen.
        ///                  As opposed to having at least 1 pixel on screen.
        ///     
        /// Returns:
        ///     Whether the object is currently visible on the screen.
        bool isOnScreen(bool completely = false)(sfFloatRect bounds)
        {
            // Convert the position to a pixel on the screen
            auto pixelPos   = this.game.view.toWindowPixel(sfVector2f(bounds.left, bounds.top));

            // Then create a rectangle of the screen
            auto screen     = sfFloatRect(0, 0, this.game.config.window.width, this.game.config.window.height);

            // Then determine if the screen contains any of the rectangle's points
            // I can probably make this a template, instead of just copy pasta-ing it   
            static if(completely)
            {
                return  sfFloatRect_contains(&screen, pixelPos.x, pixelPos.y)                   &&
                    sfFloatRect_contains(&screen, pixelPos.x + bounds.width - 1, pixelPos.y)    &&
                    sfFloatRect_contains(&screen, pixelPos.x, pixelPos.y + bounds.height - 1)   &&
                    sfFloatRect_contains(&screen, pixelPos.x + bounds.width - 1, pixelPos.y + bounds.height - 1);
            }
            else
            {
                return  sfFloatRect_contains(&screen, pixelPos.x, pixelPos.y)                   ||
                    sfFloatRect_contains(&screen, pixelPos.x + bounds.width - 1, pixelPos.y)    ||
                    sfFloatRect_contains(&screen, pixelPos.x, pixelPos.y + bounds.height - 1)   ||
                    sfFloatRect_contains(&screen, pixelPos.x + bounds.width - 1, pixelPos.y + bounds.height - 1);
            }
        }
    }
}

/// Test map object
/// 
/// Test #1: isOnScreen works. TRUE
class TestObject : MapObject
{
    private
    {
        Rectangle _rect;
    }

    public
    {
        this(Game game)
        {
            super(game);

            this._rect = new Rectangle(game, ShapeData(sfVector2f(50, 50), sfBlack));
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                this._rect.update(window, gameTime);
//                Util.log("TestObject::update", LogLevel.Debug, "On Screen = %s | NPos = %s | VPos = %s", 
//                            this.isOnScreen(this._rect.globalBounds), this._rect.position,
//                            this.game.view.worldPositionToWindowPixel(this._rect.position));
            }
        }
    }
}

/// Class for terrain objects(They're just sprites that will have collisions)
class TerrainObject : MapObject
{
    private
    {
        Rectangle   _sprite;
        string      _spritePath;
    }

    public
    {
        this(Game game, string spritePath, sfVector2f position = sfVector2f(0, 0))
        in
        {
            assert(spritePath !is null);
        }
        body
        {
            super(game);
            this._sprite = new Rectangle(game, game.assets.loadTexture(spritePath), position);
            this._spritePath = spritePath;
        }

        @property
        {
            /// Get access to the terrain's sprite, allowing you to move it and such things.
            Rectangle sprite()
            {
                return this._sprite;
            }

            /// Get the path to the image this terrain object uses.
            string spritePath()
            {
                return this._spritePath;
            }
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                this._sprite.update(window, gameTime);
            }
        }
    }
}

/// Contains a level
class Map : GameObject
{
    private
    {
        Rectangle           _background;
        string              _backgroundPath;

        TerrainObject[]     _terrain;

        /// The magic number for the map's save data
        const static ubyte[6] _magicNumber = cast(ubyte[6])"JARENA";
    }

    public
    {
        /// Create a new map.
        /// 
        /// Parameters:
        ///     game = The game object
        this(Game game)
        {
            super(game);
        }

        /// Add the given terrain object to the map.
        /// 
        /// Parameters:
        ///     object = The object to add.
        void add(TerrainObject object)
        in
        {
            assert(object !is null);
        }
        body
        {
            this._terrain ~= object;
        }

        /// Remove the terrain at the given index.
        /// 
        /// Parameters:
        ///     index = The index to remove
        void remove(size_t index)
        in
        {
            assert(index < this._terrain.length);
        }
        body
        {
             this._terrain = this._terrain.remove(index);
        }

        /// Creates and returns a ubyte[] containing enough information to load the level.
        /// 
        /// Returns:
        ///     A ubyte[] that can be used in $(B load) to reload the level.
        const(ubyte[]) save()
        {
            /*
             * String:
             * ushort-big-endian    length;
             * char[length]         data;
             * 
             * StringTable: 
             * - Contains a list of all the image files used
             * - The string table can be indexed from the rest of the data.
             * ushort-big-endian    length;
             * String[length]       images;
             * 
             * Object:
             * - Describes a TerrainObject
             * ushort-be   imageIndex; - The index in the StringTable of which image this terrain uses.
             * ushort-be   xPos;
             * ushort-be   yPos;
             * 
             * ObjectTable:
             * - Contains a list of all the terrain objects in the level.
             * ushort-be        length;
             * Object[length]   objects;
             * 
             * Format:
             * char[6]      MagicNumber = "JARENA";
             * StringTable  Images;
             * ushort-be    BackgroundImageIndex;
             * ObjectTable  Terrain;
             * 
             * */
            ubyte[] toReturn;

            // Describes an object
            struct _Object
            {
                ushort index;
                ushort x;
                ushort y;
            }

            // First, create the non-byteified form of the String and Object table
            string[]    stringTable;
            _Object[]   objectTable;

            /// Gets the index of "path" in the string table. "path" is expected to already exist.
            ushort findPath(string path)
            {
                foreach(i, _path; stringTable)
                {
                    assert(i <= ushort.max);

                    if(path == _path)
                    {
                        return cast(ushort)i;
                    }
                }

                assert(false);
            }

            stringTable ~= this._backgroundPath;

            // Go through each terrain object, and convert it into a form we can easily read in.
            foreach(terrain; this.terrain)
            {
                // Add the sprite's image path if it's not already there
                if(!stringTable.canFind(terrain.spritePath))
                {
                    stringTable ~= terrain.spritePath;
                }

                // Then create the Object to add
                auto position = terrain.sprite.position;
                objectTable ~= _Object(findPath(terrain.spritePath), cast(ushort)position.x, cast(ushort)position.y);
            }

            // Then convert it into the bytes we want
            // TODO: This is *horribly* inefficient, allocation after allocation. You can calculate the minimum bytes needed easily(And therefore pre-allocate it), so write a struct or something to handle this.

            /// Writes a number in big-endian
            void writeNumber(ushort number)
            {
                toReturn ~= nativeToBigEndian(number);
            }

            /// WRites a length-prefixed string.
            void writeString(string data)
            {
                writeNumber(cast(ushort)data.length);
                toReturn ~= cast(ubyte[])data;
            }

            // Magic number
            toReturn ~= Map._magicNumber;

            // String table
            writeNumber(cast(ushort)stringTable.length);
            foreach(entry; stringTable)
            {
                writeString(entry);
            }

            // The index of the background image.
            writeNumber(cast(ushort)findPath(this._backgroundPath));

            // Object table
            writeNumber(cast(ushort)objectTable.length);
            foreach(object; objectTable)
            {
                writeNumber(object.index);
                writeNumber(object.x);
                writeNumber(object.y);
            }

            return toReturn;
        }

        /// Loads the given map data.
        /// 
        /// Parameters:
        ///     data = The save data to laod.
        void load(ubyte[] data)
        {
            // Clear the map
            this._terrain.length = 0;

            // Holds all of the images to load in.
            string[] stringTable;

            // Check for the magic number
            size_t index = 0;

            /// Reads in the specified amount of bytes.
            ubyte[] readBytes(size_t amount, uint line = __LINE__)
            {
                index += amount;

                // If we're out of bounds, abort
                if((index - amount) > data.length)
                {
                    Util.log("Map::load::readBytes", LogLevel.Error,
                        "On line %s: Unable to load map. Reason: Out of bounds(either a bug, or invalid map data).
                        index = %s | amount = %s", line, index, amount);
                }

                return data[index - amount..index];
            }

            /// Reads a number from the file.
            ushort readNumber(uint line = __LINE__)
            {
                enum auto size = ushort.sizeof;
                return bigEndianToNative!ushort(cast(ubyte[size])readBytes(size, line)[0..size]);
            }

            /// Reads a string from the file.
            string readString(uint line = __LINE__)
            {
                auto length = readNumber(line);
                return cast(string)readBytes(length, line);
            }

            // Make sure the magic number is correct.
            if(readBytes(Map._magicNumber.length) != Map._magicNumber)
            {
                Util.log("Map::load", LogLevel.Error, "Invalid map data. Reason: Magic number");
                return;
            }

            // Read in the string table
            stringTable.length = readNumber();
            foreach(i; 0..stringTable.length)
            {
                stringTable[i] = readString();
            }
            Util.log("Map::load", LogLevel.Info, "Number of Images = %s. Images = %s", stringTable.length, stringTable);

            // Set the background
            this.background = stringTable[readNumber()];

            // Read in the terrain
            auto length = readNumber();
            foreach(i; 0..length)
            {
                // TODO: WRite out a byte specifying what type of terrain it is.
                // E.G. 0 = Physical terrain. 1 = Spawn point. 2 = Platform, etc.

                auto imageIndex = readNumber();
                auto x          = readNumber();
                auto y          = readNumber();

                Util.log("Map::load", LogLevel.Debug, "New terrain object(%s/%s): image = %s[%d] | x = %s | y = %s", 
                    i, length, stringTable[imageIndex], imageIndex, x, y);
                this.add(new TerrainObject(this.game, stringTable[imageIndex], sfVector2f(x, y)));
            }

            Util.log("Map::load", LogLevel.Info, "Number of terrain objects = %s", this.terrain.length);
            Util.log("Map::load", LogLevel.Info, "Loaded level.");
        }

        @property
        {
            /// Get the map's terrain.
            TerrainObject[] terrain()
            {
                return this._terrain;
            }

            /// Get the map's background.
            Rectangle background()
            {
                return this._background;
            }

            /// Set the map's background
            void background(string path)
            {
                this._background        = new Rectangle(this.game, this.game.assets.loadTexture(path));
                this._backgroundPath    = path;
            }
        }

        override
        {
            /// Update the object.
            /// 
            /// Parameters:
            ///     window = The game's window
            ///     gameTime = The time the last frame took
            void update(sfRenderWindow* window, sfTime gameTime)
            {
                // Draw the background and the terrain
                if(this._background !is null)
                {
                    this._background.update(window, gameTime);
                }

                foreach(object; this._terrain)
                {
                    object.update(window, gameTime);
                }
            }
        }
    }
}