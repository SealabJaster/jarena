﻿module jarena.object;

public
{
	import std.typecons;

	import derelict.sfml2.graphics, derelict.sfml2.system, derelict.sfml2.window;

	import jarena.config, jarena.util, jarena.game, jarena.input, jarena.gui, jarena.map, jarena.view, jarena.physics;
}

/// The base class for all objects
class GameObject
{
	private
	{
		Game	_game;
	}

	public
	{
		/// This ensures that any inheriting class will always have a reference to the game
		this(Game game)
		in
		{
			assert(game !is null);
		}
		body
		{
			this._game = game;
		}

		abstract
		{
			/// Update the object.
			/// 
			/// Parameters:
			/// 	window = The game's window
			/// 	gameTime = The time the last frame took
			void update(sfRenderWindow* window, sfTime gameTime);
		}
	}

	protected
	{
		/// Get a reference to the game
		@property
		Game game()
		{
			return this._game;
		}
	}
}