﻿module jarena.gamestate;

private
{
    import std.conv, std.math, std.file, std.path;
    import std.algorithm : canFind;

	import jarena.object;
}

/// Contains the states of the game
enum GameStates
{
	/// The main menu
	Menu_Main,

	/// The testing game state
	Testing,

    /// The state that allows you to edit maps.
    MapEditor
}

/// The base class for a game state.
class GameState : GameObject
{
	private
	{
		GameStates	_state;
	}

	public
	{
		/// Each inheriting class requires to specify which state it represents
		this(Game game, GameStates state)
		{
			super(game);
			this._state = state;
		}

		/// Get what state this class represents.
		@property
		GameStates representedState()
		{
			return this._state;
		}
	}
}

/// A container for game states.
class GameStateManager : GameObject
{
	private
	{
		GameState[GameStates]	_states;
		GameState				_current;
	}

	public
	{
		/// Constructs a gamestate manager
		this(Game game)
		{
			super(game);
		}

		/// Adds a game state. It can then be accessed using the GameStates enum, using the state it represents.
		/// 
		/// Parameters:
		/// 	state = The game state to add.
		void add(GameState state)
		in
		{
			assert(state !is null);
			assert((state.representedState in this._states) is null);
		}
		body
		{
			Util.log("GameStateManager::add", LogLevel.Info, 
					 "Adding game state for the '%s' state.", state.representedState);
			this._states[state.representedState] = state; // P.S. "state"

			if(this._current is null)
			{
				this.swap(state.representedState);
			}
		}

		/// Convienence method to add a game state. Assumes that it has a constructor for just a Game object.
		/// 
		/// Parameters:
		/// 	T(template) = The game state's type, which will then be created.
		void add(T : GameState)()
		{
			this.add(new T(this.game));
		}

		/// Get the desired game state that represents a certain state.
		/// 
		/// Parameters:
		/// 	state = The state to get the game state for.
		/// 
		/// Returns:
		/// 	The game state that represents $(B state)
		GameState get(GameStates state)
		in
		{
			assert((state in this._states) !is null);
		}
		body
		{
			Util.log("GameStateManager::get", LogLevel.Debug, "Getting the game state for the '%s' state.", state);
			return this._states[state];
		}

		/// Swaps the currently active game state.
		/// 
		/// Parameters:
		/// 	state = The state to swap the game to
		void swap(GameStates state)
		{
			// Clear the mouse data, just incase it may cause something to instantly activate
			this.game.mouse.clearButtons();
            core.memory.GC.collect();

			this._current = this.get(state);
		}

		/// Updates the currently active game state.
		/// 
		/// Parameters:
		/// 	window = The game's window
		/// 	gameTime = The time the last frame took
		override void update(sfRenderWindow* window, sfTime gameTime)
		{
			this._current.update(window, gameTime);
		}
	}
}

/// The game state for the main menu.
class MainMenuState : GameState
{
    private
    {
        GuiContainer    _ui;
        bool            _close = false;

        /// Setup the main menu's ui
        void setupUI(Game game)
        {
            this._ui = new GuiContainer(game, 
                sfFloatRect(0, 0, this.game.config.window.width, this.game.config.window.height));

            // Default look of buttons
            auto buttonShape = ShapeData(
                sfVector2f(this.game.config.window.width * 0.25f, 100),
                sfColor(128, 128, 0, 255),
                2,
                sfColor(0, 0, 0, 255));
            auto buttonLabel = LabelData(
                24,
                this.game.config.fonts.main,
                sfWhite
                );
            float offset = (buttonShape.size.y + (buttonShape.thickness * 2));

            auto exit = new Button(game,
                buttonShape,
                buttonLabel,
                "Exit Game",
                &this.eventExit
                );
            exit.centerIn(this._ui);
            exit.position = sfVector2f(exit.position.x, 
                (this.game.config.window.height) - offset);

            auto editor = new Button(game,
                buttonShape,
                buttonLabel,
                "Level Editor",
                &this.eventToEditor
                );
            editor.position = exit.position.sub(sfVector2f(0, offset));

            this._ui.add(exit,      "Exit");
            this._ui.add(editor,    "Level Editor");
        }

        /// Exits the game
        void eventExit(Button sender)
        {
            this._close = true;
        }

        void eventToEditor(Button sender)
        {
            this.game.stateManager.swap(GameStates.MapEditor);
        }
    }

    public
    {
        this(Game game)
        {
            super(game, GameStates.Menu_Main);

            this.setupUI(game);
        }

        /// Update the object.
        /// 
        /// Parameters:
        ///     window = The game's window
        ///     gameTime = The time the last frame took
        override void update(sfRenderWindow* window, sfTime gameTime)
        {
            // Make sure we're not using the custom camera
            if(!this.game.view.useDefaultView)
            {
                this.game.view.useDefaultView = true;
            }

            this._ui.update(window, gameTime);

            // If _close has been flagged, then close the game window.
            // WHich pretty much ends the game without issues.
            if(this._close)
            {
                sfRenderWindow_close(window);
            }
        }
    }
}

/// The game state dedicated to testing new game code.
class TestingState : GameState
{
	private debug
	{
		Timer 			_timer;
		GuiContainer	_container;

		void testTimer(sfTime gameTime)
		{
			this._timer.update(gameTime);

			// Tick every 5 seconds or when the left mouse button is clicked
			if(this._timer.tick || this.game.mouse.isClicked(sfMouseLeft))
			{
				Util.log("TestingState::testTimer", LogLevel.Debug, "Ticked. gameTime(seconds) = %s. timer = %s.",
					sfTime_asSeconds(gameTime), this._timer);

				Util.log("TestingState::testTimer", LogLevel.Debug, "Mouse = %s. Left(pressed) = %s. Left(clicked) = %s",
					this.game.mouse.state, this.game.mouse.isPressed(sfMouseLeft), this.game.mouse.isClicked(sfMouseLeft));
			}
		}

        void testEvent(Button sender)
        {
            Util.log("TestingState::testEvent", LogLevel.Debug, "Event called.");
        }

		void testGui(sfRenderWindow* window, sfTime gameTime)
		{
			this._container.update(window, gameTime);

            // Pixels/second
            auto speed = (50f * sfTime_asSeconds(gameTime));
			// Test the movement stuff of the container(Main thing I care about).
			if(sfKeyboard_isKeyPressed(sfKeyLeft))
			{
				this._container.move(sfVector2f(-speed, 0));
			}
			else if(sfKeyboard_isKeyPressed(sfKeyRight))
			{
				this._container.move(sfVector2f(speed, 0));
			}

            // Test the view moving
            this.game.view.bounds = Bounds(-1318, -840, 
                                        this.game.config.window.width, this.game.config.window.height);
            if(sfKeyboard_isKeyPressed(sfKeyD))
            {
                this.game.view.move(sfVector2f(-speed, 0));
            }
            else if(sfKeyboard_isKeyPressed(sfKeyA))
            {
                this.game.view.move(sfVector2f(speed, 0));
            }

			// Test the textbox
			auto box = this._container.get!Textbox("Text");
            if(box.input.text == "Dan is a chode")
			{
				Util.log("TestingState::testGui", LogLevel.Debug, "I approve!");
            }

            // Test changing the sprite's y axis
            if(sfKeyboard_isKeyPressed(sfKeyTab))
            {
                auto sheet = this._container.get!SpriteSheet("Sprite");
                sheet.frameY = (sheet.frameY == 1) ? 0 : 1;
            }

            // Test some physics stuff
            auto rect = this._container.get!Rectangle("Rect");
            if(sfKeyboard_isKeyPressed(sfKeyE))
            {
                rect.move(sfVector2f(speed, 0));
            }
            else if(sfKeyboard_isKeyPressed(sfKeyQ))
            {
                rect.move(sfVector2f(-speed, 0));
            }
            //Util.log("TestingState::testGui", LogLevel.Debug, "Rect Pos(View.toWorldPixel) = %s", 
            //      this.game.view.toWindowPixel(rect.position));

            sfFloatRect intersection;
            auto collision = Physics.isCollision(box.globalBounds, rect.globalBounds, &intersection);
//            Util.log("TestingState::testGui", LogLevel.Debug, "Collided = %s | Intersection = %s", 
//                        collision, intersection);

            //Util.log("TestingState::testGui", LogLevel.Debug, "Pos = %s | Bounds = %s", rect.position, rect.globalBounds);

//			Util.log("TestingState::testGui", LogLevel.Debug, "TestLabel's position = %s.", 
//					this._container.get!GuiObject("TestLabel").position);
		}

		void setupGui()
		{
            this._timer.timeUntilTick = 5;

			// Setup the container to cover the entire window
			this._container = new GuiContainer(this.game, sfFloatRect(0, 0, 
															game.config.window.width, game.config.window.height));

			// Create a label that is(mostly, because SFML and text) centered in the container
			auto label = new Label(this.game, LabelData(24, this.game.config.fonts.main, sfBlack),
									"This is a string of sexiness.");
			label.centerIn(this._container);

            // Create a rectangle
            auto rect = new Rectangle(this.game, this.game.assets.loadTexture("Assets/Test/Tahn.png"));

            // Create a textbox
            auto text = new Textbox(this.game, ShapeData(sfVector2f(400, 200), sfBlack, 0), 
                                    LabelData(24, this.game.config.fonts.main, sfWhite));
            text.centerIn(this._container, sfVector2f(100, -200));

            // Create a button
            auto button = new Button(this.game, 
                                    ShapeData(sfVector2f(200, 100), sfBlue, 2, sfBlack),
                                    LabelData(18, this.game.config.fonts.main, sfWhite),
                                    "Click here",
                                    &this.testEvent);
            button.centerIn(this._container, sfVector2f(0, 200));

            // Create the FPS counter
            auto fps = new FPSCounter(game, LabelData(18, this.game.config.fonts.main, sfBlack), sfVector2f(0, 500));

            // Create a sprite sheet
            auto sheet = new SpriteSheet(game, this.game.assets.loadTexture("Assets/Test/Sheet.png"), sfVector2u(32, 32));
            sheet.start(2);
            sheet.move(sfVector2f(0, 380));

			// Then add the controls
			this._container.add(label,  "TestLabel",false);
            this._container.add(rect,   "Rect",     false);
            this._container.add(text,   "Text",     false);
            this._container.add(button, "Button",   false);
            this._container.add(fps,    "FPS",      false);
            this._container.add(sheet,  "Sprite",   false);
		}

        void testMapSaving()
        {
            // As a quick and dirty test(until I have made a map editing gamestate) we'll just do this
            auto map = new Map(this.game);
            map.background = "Assets/Test/Background.png";
            map.add(new TerrainObject(this.game, "Assets/Test/Tahn.png"));
            map.add(new TerrainObject(this.game, "Assets/Test/Tahn.png",  sfVector2f(500, 200)));
            map.add(new TerrainObject(this.game, "Assets/Test/Sheet.png", sfVector2f(0, 300)));
            map.add(new TerrainObject(this.game, "Assets/Test/Sheet.png", sfVector2f(256, 0)));

            // Current verdict = WORKING
            //std.file.write("Test.dat", map.save());
        }

        // Another quick and dirty test that will be deleted later when a map editor game state is made
        // VERDICT = WORKING
        Map _map;
        bool _loaded = false;
        void testMapLoading(sfRenderWindow* window, sfTime gameTime)
        {
            if(!this._loaded)
            {
                this._loaded = true;
                this._map = new Map(this.game);
                //this._map.load(cast(ubyte[])std.file.read("Test.dat"));
            }

            this._map.update(window, gameTime);
        }
	}

	public
	{
		this(Game game)
		{
			super(game, GameStates.Testing);

            debug
            {
    			this.setupGui();
                this.testMapSaving();
            }
        }

		/// Updates the game state.
		/// 
		/// Parameters:
		/// 	window = The game's window
		/// 	gameTime = The time the last frame took
		override void update(sfRenderWindow* window, sfTime gameTime)
		{
            debug
            {
    			this.testTimer(gameTime);
                this.testMapLoading(window, gameTime);
    			this.testGui(window, gameTime);
            }
        }
	}
}

/// The game state to edit maps.
class MapEditorState : GameState
{
    private
    {
        /// Contains the map
        Map             _map;

        /// This container contains the controls for the input Gui.
        /// The Input Gui is used anytime text input is needed.
        GuiContainer    _inputGui;

        /// This container holds all of the controls for accessing all the other guis.
        GuiContainer    _mainGui;

        /// Array of images that the map can use as terrain.
        string[]        _terrainImages;
        size_t          _currentImage; // The index of the current image.
        Rectangle       _previewImage; // Boobs.

        void setupGui(Game game)
        {
            this.setupMainGui(game);
            this.setupInput(game);
        }

        void setupMainGui(Game game)
        {
            this._mainGui = new GuiContainer(game, 
                sfFloatRect(0, 0, game.config.window.width, game.config.window.height));

            // General data for the buttons
            auto buttonShape = ShapeData(sfVector2f(140, 30), sfBlue, 3, sfBlack);
            auto buttonLabel = LabelData(18, game.config.fonts.main, sfWhite);

            auto setBackground = new Button(game,
                    buttonShape,
                    buttonLabel,
                    "Set Background",
                    &this.setBackgroundInput
                );
            setBackground.position = sfVector2f(
                    5,
                    (game.config.window.height - buttonShape.size.y) - 5
                );

            auto back = new Button(game,
                buttonShape,
                buttonLabel,
                "Main Menu",
                &this.goBack
                );
            back.position = sfVector2f(
                (game.config.window.width - buttonShape.size.x) - 5,
                setBackground.position.y
                );

            auto save = new Button(game,
                    buttonShape,
                    buttonLabel,
                    "Save Map",
                    &this.saveMapInput,
                    setBackground.position.add(sfVector2f(buttonShape.size.x + 10, 0))
                );

            auto load = new Button(game,
                buttonShape,
                buttonLabel,
                "Load Map",
                &this.loadMapInput,
                save.position.add(sfVector2f(buttonShape.size.x + 10, 0))
                );

            auto addTerrain = new Button(game,
                buttonShape,
                buttonLabel,
                "Add Terrain",
                &this.addTerrainInput,
                setBackground.position.sub(sfVector2f(0, buttonShape.size.y + 10))
                );

            auto fps            = new FPSCounter(game, LabelData(18, game.config.fonts.main, sfColor(128, 128, 128, 255)));
            this._previewImage  = new Rectangle(game, ShapeData(sfVector2f(0,0), sfColor(255, 255, 255, 128)));

            this._mainGui.add(setBackground,    "Set Background");
            this._mainGui.add(fps,              "FPS");
            this._mainGui.add(save,             "Save Map");
            this._mainGui.add(load,             "Load Map");
            this._mainGui.add(addTerrain,       "Add Terrain");
            this._mainGui.add(_previewImage,    "Preview");
            this._mainGui.add(back,             "Back");
        }

        void setupInput(Game game)
        {
            // Setup the input gui
            this._inputGui = new GuiContainer(game, 
                sfFloatRect(0, 0, game.config.window.width, game.config.window.height));
            this._inputGui.active = true;
            
            auto textbox = new Textbox(game, 
                ShapeData(sfVector2f(game.config.window.width * 0.75, 40), sfBlack, 0, sfBlack),
                LabelData(20, game.config.fonts.main, sfWhite)
                );
            textbox.centerIn(this._inputGui);
            
            // These are shared between 2 buttons, so it's nicer to store them in variables.
            auto buttonShape = ShapeData(sfVector2f(80, 30), sfBlue, 3, sfBlack);
            auto buttonLabel = LabelData(18, game.config.fonts.main, sfWhite);
            
            auto confirm = new Button(game,
                buttonShape,
                buttonLabel,
                "",
                &this.cancelInput
                );

            auto textboxPos = textbox.position;
            confirm.position = sfVector2f(
                (textboxPos.x + textbox.globalBounds.width) - confirm.globalBounds.width,
                (textboxPos.y + textbox.globalBounds.height) + 5
                );
            
            auto cancel = new Button(game,
                buttonShape,
                buttonLabel,
                "Cancel",
                &this.cancelInput
                );
            cancel.position = sfVector2f(
                textboxPos.x,
                (textboxPos.y + textbox.globalBounds.height) + 5
                );
            
            this._inputGui.add(textbox, "Input");
            this._inputGui.add(confirm, "Confirm");
            this._inputGui.add(cancel,  "Cancel");
            this._inputGui.active = false;
        }

        // Events //
        void cancelInput(Button sender)
        {
            this._inputGui.active = false;
            this.game.mouse.clearButtons();
        }

        void setBackgroundInput(Button sender)
        {
            this.getInput("Set", &this.setBackground);
        }
        void setBackground(Button sender)
        {
            // Convert the dstring to a normal string, make sure it exists, then boobs.
            auto input = this.input;
            if(!std.file.exists(input))
            {
                return;
            }

            this._map.background = input;

            // Fix the viewport's view to only allow it to move within the background
            this.fixViewport();
            this.cancelInput(sender);
        }

        void saveMapInput(Button sender)
        {
            this.getInput("Save", &this.saveMap);
        }
        void saveMap(Button sender)
        {
            // Paths are relative to [exePath]/Assets/Maps
            auto input = buildPath(
                ["Assets", "Maps", 
                    this.input.setExtension("dat")]);

            Util.log("MapEditorState::saveMap", LogLevel.Info, "Saving map to '%s'", input);

            std.file.write(input, this._map.save);
            this.cancelInput(sender);
        }

        void goBack(Button sender)
        {
            this.game.stateManager.swap(GameStates.Menu_Main);
        }

        void loadMapInput(Button sender)
        {
            this.getInput("Load", &this.loadMap);
        }
        void loadMap(Button sender)
        {
            // Paths are relative to [exePath]/Assets/Maps
            auto input = buildPath(
                ["Assets", "Maps", 
                    this.input.setExtension("dat")]);

            this._map.load(cast(ubyte[])std.file.read(input));
            this.fixViewport();

            // Register all of the terrain
            this._terrainImages.length  = 0;
            this._currentImage          = 0;
            foreach(terrain; this._map.terrain)
            {
                if(!this._terrainImages.canFind(terrain.spritePath))
                {
                    this._terrainImages ~= terrain.spritePath;
                }
            }

            this.updatePreview();
            this.cancelInput(sender);
        }

        void addTerrainInput(Button sender)
        {
            this.getInput("Add", &this.addTerrain);
        }
        void addTerrain(Button sender)
        {
            if(!this._terrainImages.canFind(this.input) && exists(this.input))
            {
                // Add the image, then move our current image to it
                this._terrainImages ~= this.input;
                this._currentImage  = (this._terrainImages.length - 1);
                this.updatePreview();
            }

            this.cancelInput(sender);
        }
        // Non events //

        /// Makes the input gui visible, allows you to change the text and event of the Confirm button.
        void getInput(dstring text, ButtonEvent event)
        in
        {
            assert(event !is null);
        }
        body
        {
            auto button         = this._inputGui.get!Button("Confirm");
            button.text.text    = text;
            button.event        = event;

            this._inputGui.active = true;
        }

        // Display the gui
        void updateGui(sfRenderWindow* window, sfTime gameTime)
        {
            this._mainGui.update(window, gameTime);

            if(this._inputGui.active)
            {
                this._inputGui.update(window, gameTime);
            }
        }

        /// Move the camera
        void cameraControls(sfRenderWindow* window, sfTime gameTime)
        {
            auto speed = (250f * sfTime_asSeconds(gameTime));
            if(sfKeyboard_isKeyPressed(sfKeyD))
            {
                this.game.view.move(sfVector2f(speed, 0));
            }
            if(sfKeyboard_isKeyPressed(sfKeyA))
            {
                this.game.view.move(sfVector2f(-speed, 0));
            }
            if(sfKeyboard_isKeyPressed(sfKeyW))
            {
                this.game.view.move(sfVector2f(0, -speed));
            }
            if(sfKeyboard_isKeyPressed(sfKeyS))
            {
                this.game.view.move(sfVector2f(0, speed));
            }
        }

        /// Fixes the viewport so it can't go out of bounds
        void fixViewport()
        {
            auto bounds = this._map.background.globalBounds;
            this.game.view.bounds = Bounds(
                0,
                0,
                bounds.width,
                fmax(game.config.window.height, bounds.height)
                );
        }

        /// ADONAGINAOSG Mouse controls.
        void mouseControls(sfRenderWindow* window, sfTime gameTime)
        {
            // Change the image of the terrain depending on if the mouse wheel was moved.
            if(this.game.mouse.state.wheelDelta > 0)
            {
                this._currentImage += (this._currentImage + 1 >= this._terrainImages.length) ?
                                        -this._currentImage :
                                        1;
            }
            else if(this.game.mouse.state.wheelDelta < 0)
            {
                this._currentImage -= (this._currentImage == 0) ?
                                        -(this._terrainImages.length - 1) :
                                        1;
            }

            // Only bother showing the image if we have terrain, and if there was a change in the mouse
            if(this.game.mouse.state.wheelDelta != 0)
            {
                this.updatePreview();
            }

            // Center the rectangle to the mouse
            auto mousePos   = this.game.mouse.state.position;
            auto rectPos    = sfVector2f(0, 0).add(mousePos);
            auto rectBounds = this._previewImage.globalBounds;
            auto rectSize   = sfVector2f(rectBounds.width, rectBounds.height);
            rectPos         = rectPos.sub(rectSize.div(sfVector2f(2, 2)));

            this._previewImage.position = rectPos;

            // If the left mouse button was clicked, add the thing
            if(this.game.mouse.isClicked(sfMouseLeft) && this._terrainImages.length > 0)
            {
                rectPos = this.game.view.toWorldPixel(sfVector2i(cast(int)rectPos.x, cast(int)rectPos.y));
                this._map.add(new TerrainObject(game, this._terrainImages[this._currentImage], rectPos));
            }

            // However, if the right mouse button was clicked, remove the first object found that is over the mouse.
            // (If we go in reverse, then we can remove the latest object added <3)
            if(this.game.mouse.isClicked(sfMouseRight))
            {
                ptrdiff_t toDelete = -1;
                auto mouseWorldPos = this.game.view.toWorldPixel(mousePos);

                foreach_reverse(i, terrain; this._map.terrain)
                {
                    auto bounds = terrain.sprite.globalBounds;
                    if(sfFloatRect_contains(&bounds, mouseWorldPos.x, mouseWorldPos.y))
                    {
                        toDelete = i;
                        break;
                    }
                }

                if(toDelete >= 0)
                {
                    this._map.remove(toDelete);
                }
            }

//            Util.log("MapEditorState::mouseControls", LogLevel.Debug, "Preview Position = %s | Bounds = %s | Size = %s", 
//                this._previewImage.position, rectBounds, rectSize);
        }

        /// Updates the image of the preview.
        void updatePreview()
        {
            if(this._terrainImages.length > 0)
            {
                this._previewImage.texture = this.game.assets.loadTexture(this._terrainImages[this._currentImage]);
            }
        }

        /// Get the text from the input box
        @property
        string input()
        {
            return to!string(this._inputGui.get!Textbox("Input").input.text);
        }
    }

    public
    {
        this(Game game)
        {
            super(game, GameStates.MapEditor);
            this._map           = new Map(game);

            this.setupGui(game);
        }

        /// Updates the game state.
        /// 
        /// Parameters:
        ///     window = The game's window
        ///     gameTime = The time the last frame took
        override void update(sfRenderWindow* window, sfTime gameTime)
        {
            // Make sure we're not using the default camera
            if(this.game.view.useDefaultView)
            {
                this.game.view.useDefaultView = false;
            }

            this._map.update(window, gameTime);
            this.updateGui(window, gameTime);
            this.cameraControls(window, gameTime);
            this.mouseControls(window, gameTime);
        }
    }
}